<div align="center" style="font-size: 7em">

<img src="https://img.icons8.com/emoji/192/000000/memo-emoji.png"/>
</div>

<div align="center">
<h1>Blockchain contracts</h1>
</div>


## Description

Smart contracts of [BlockCOVID](https://sweleven.gitlab.io/blockcovid/) to store proofs of bookings and sanitizations built with <a href="https://www.trufflesuite.com/truffle">Truffle</a>.


## Stay in touch

- Author - [Filippo Pinton](https://gitlab.com/Butterneck.com)
- Website - [https://sweleven.gitlab.io](https://sweleven.gitlab.io/)
